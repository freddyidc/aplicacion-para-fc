import { Component,ViewChild } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ListarCasosPage } from '../listar-casos/listar-casos';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  @ViewChild('myNav') nav: NavController
  constructor(public navCtrl: NavController) {
  }

  public verCasos(){
    this.navCtrl.push(ListarCasosPage);
  }

}
