import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {CasosService} from '../../services/casos.service';
/**
 * Generated class for the VerCasoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-ver-caso',
  templateUrl: 'ver-caso.html',
})
export class VerCasoPage {
  caso = {id:null, detalles:null};
  id = null;
  observacion = null;
  constructor(public navCtrl: NavController, public navParams: NavParams, public casosService: CasosService) {
    this.id = navParams.get('id')
    this.caso = casosService.getCaso(this.id);
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad VerCasoPage');
  }

  public agregarObservacion(){
    this.casosService.agregarObservacion(this.id, this.observacion);
  }
}
