import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VerCasoPage } from './ver-caso';

@NgModule({
  declarations: [
    VerCasoPage,
  ],
  imports: [
    IonicPageModule.forChild(VerCasoPage),
  ],
})
export class VerCasoPageModule {}
