import { Component,ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {CasosService} from '../../services/casos.service';
import { VerCasoPage } from '../ver-caso/ver-caso';
//import {VerCasoPage} from '../ver-caso/ver-caso';
/**
 * Generated class for the ListarCasosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-listar-casos',
  templateUrl: 'listar-casos.html',
})
export class ListarCasosPage {
  casos = [];
  constructor(public navCtrl: NavController, public navParams: NavParams, public casosService: CasosService) {
    this.casos = casosService.getCasos();
  }

  public verCaso(id){
    this.navCtrl.push(VerCasoPage, {id:id});
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ListarCasosPage');
  }



}
