import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListarCasosPage } from './listar-casos';

@NgModule({
  declarations: [
    ListarCasosPage,
  ],
  imports: [
    IonicPageModule.forChild(ListarCasosPage),
  ],
})
export class ListarCasosPageModule {}
