import {Injectable} from '@angular/core'

@Injectable()
export class CasosService{
  casos = [
    {id: 1, detalles: 'uno'},
    {id: 2, detalles: 'dos'},
    {id: 3, detalles: 'tres'}
  ];
  caso = {id:null, detalles:null};
  public getCasos(){
    return this.casos;
  }

  public getCaso(id){
    return this.casos.filter(function(e,i){ return e.id == id})[0] || {id:null, detalles:null};
  }


  public agregarObservacion(id, observacion){
    this.caso = this.getCaso(id);
    this.caso.detalles = this.caso.detalles + "\n" + observacion;
  }
}
